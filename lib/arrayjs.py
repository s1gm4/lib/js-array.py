#!/usr/bin/env python3

class ArrayJS:
    def __init__(self, data):
        self.data = data

    @property
    def length(self):
        return len(self.data)

    def push(item):
        self.data.append(item)

    def pop():
        return self.data.pop()

    def filter(self, condition):
        keeped = []
        for item in data:
            if condition(item):
                keeped.append(item)
        return ArrayJS(keeped)
    
    def forEach(self, function):
        for item, index in enumerate(data):
            function(data, item, this.data)
    
    def shift(self):
        if (self.length > 0):
            item = self.data[0]
            if self.length > 1:
                self.data = self.data[1:]
        else:
            return None

    def unshift(self, item):
        self.data = [item] + self.data

    def indexOf(self, item):
        return self.data.index(item)

    def splice(self, start, end):
        returned = self.data[start:end]
        self.data = self.data[:start] + self.data[end:]
        return ArrayJS(returned)
    
    def slice(self):
        pass

    def at(self, index):
        if index < self.length:
            return self.data[index]
        else:
            return None

    def concat(self, other):
        return ArrayJS(self.data + other.data)

    def every(self, condition):
        for item in self.data:
            if not condition(item):
                return False
        else:
            return True

    def some(self, condition):
        for item in self.data:
            if condition(item):
                return true
        else:
            return False

    def find(self, condition):
        for item in self.data:
            if condition(item):
                return item
        else:
            return None

    def findIndex(self, condition):
        for item, index in enumerate(self.data):
            if condition(item):
                return index
        return -1

    def includes(self, value):
        return value in self.data

    def join(self, *args):
        separator = ","
        if len(args) > 0:
            separator = args[0]
        return separator.join(self.data)

    def lastIndexOf(self, target):
        for index in range(self.length - 1, -1, -1):
            if self.data[index] == target:
                return index
        else:
            return -1

    def map(self, function):
        result = []
        for item in self.data:
            result.append(function(item))
        return ArrayJS(result)

    def reduce(self, function, *args):
        if len(args) > 0:
            initValue = args[0]
        else:
            initValue = None
        result = initValue if not initValue is None else self.data[0]

        for index in range(self.length):
            if (initValue == None and index == 0):
                continue
            else:
                value = self.data[index]
                result = function(result, value, index)
        return result
        
    def reverse(self):
        rev = self.data[::-1]
        return ArrayJS(rev)

    def __repr__(self):
        return self.toString()

    def toString(self):
        for index in range(self.length - 1)

def main():
    arr = ArrayJS(range(10))
    print(arr.reduce(lambda acc, curr, index : acc + curr, 0))

if __name__ == "__main__":
    main()